package com.example.myroomapp.fragment.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myroomapp.R
import com.example.myroomapp.model.User
import kotlinx.android.synthetic.main.view_list.view.*

class ListAdapter:RecyclerView.Adapter<ListAdapter.MyViewHolder>(){

    private var userList = emptyList<User>()
    class MyViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list,parent,false))
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = userList[position]
        holder.itemView.textview_list_id.text = currentItem.id.toString()
        holder.itemView.textview_list_firstname.text = currentItem.firstName
        holder.itemView.textview_list_lastname.text = currentItem.lastName
        holder.itemView.textview_list_age.text = currentItem.age.toString()

        holder.itemView.rowLayout.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }
    fun setData(user:List<User>){
        this.userList = user
        notifyDataSetChanged()
    }
}